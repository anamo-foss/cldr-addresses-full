# CLDR Addresses Full

This repository provides the addresses portion of the JSON distribution of CLDR locale data for internationalization.

This repository belongs to the Unicode Common Locale Data Repository (CLDR) Project from Anamo.

It builds uppon data by Google's [googlei18n/libaddressinput](https://github.com/googlei18n/libaddressinput/wiki/AddressValidationMetadata) repository.
